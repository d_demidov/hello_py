#include <vector>
#include <iostream>

#include <omp.h>

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

namespace py = pybind11;

void print(py::object obj) {
    auto coo = py::cast<std::vector<double>>(obj.attr("coo"));
    for(auto x : coo) std::cout << x << std::endl;
}

double omp_sum(const std::vector<double> &x) {
    std::cout << "omp threads: " << omp_get_num_procs() << std::endl;
    double sum = 0;

#pragma omp parallel for reduction(+:sum)
    for(int i = 0; i < x.size(); ++i)
        for(int j = 0; j < 100000; ++j)
            sum += sin(x[i] * j);

    return sum;
}

PYBIND11_PLUGIN(hello) {
    py::module m("hello");

    m.def("print", print);
    m.def("omp_sum", omp_sum);

    return m.ptr();
}
