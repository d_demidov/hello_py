#!/usr/bin/python
import hello

class C:
    def __init__(self, x, y):
        self.coo = [x,y]

c = C(1,2)

hello.print(c)

v = [1] * 1024

print(hello.omp_sum(v))
